### Keybase proof

I hereby claim:

  * I am greemdev on github.
  * I am greem (https://keybase.io/greem) on keybase.
  * I have a public key ASB0OUE1jzoopV9FPkmQp0z2SW9KnwXwtlRnVPnxu7Jxawo

To claim this, I am signing this object:

```json
{
  "body": {
    "key": {
      "eldest_kid": "0120743941358f3a28a55f453e4990a74cf6496f4a9f05f0b6546754f9f1bbb2716b0a",
      "host": "keybase.io",
      "kid": "0120743941358f3a28a55f453e4990a74cf6496f4a9f05f0b6546754f9f1bbb2716b0a",
      "uid": "7bf9ce8202fe0a67cff9202e34aed219",
      "username": "greem"
    },
    "merkle_root": {
      "ctime": 1571379261,
      "hash": "ce6e255e18f395659a686c00c25aef29666c07f61ce8081a139942e1dd431191a5c093406114aaa3057010d06939c1112a9c7c26f6e193bb931e3a6055c95254",
      "hash_meta": "6eaa041fc0a72280dd5920b40ef681725f5e1d413659d179a100c54c2f5ae6ff",
      "seqno": 8210813
    },
    "service": {
      "entropy": "TRHO+gTf0xx1VilpSvDoPL4F",
      "name": "github",
      "username": "greemdev"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "4.6.1"
  },
  "ctime": 1571379271,
  "expire_in": 504576000,
  "prev": "912d040c7fe333a8cd591d3d15d76ddf0bf1333148e0e023c68db48b88b8eb43",
  "seqno": 4,
  "tag": "signature"
}
```

with the key [ASB0OUE1jzoopV9FPkmQp0z2SW9KnwXwtlRnVPnxu7Jxawo](https://keybase.io/greem), yielding the signature:

```
hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgdDlBNY86KKVfRT5JkKdM9klvSp8F8LZUZ1T58buycWsKp3BheWxvYWTESpcCBMQgkS0EDH/jM6jNWR09Fddt3wvxMzFI4OAjxo20i4i460PEIIQmKHkuEyWPZSZswrkuD++/AKklceIIzXpPJt0SxasWAgHCo3NpZ8RAqmGG1ADjfk/rQaDcKZd4BUhxWIr4WkrN/mkcoxkpf2py9vXOq0+M3tA6cKOxYgMUtxlfhAEE2TUzaNUoVYL3DqhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIBET22GErHoxwE8dEu+fk94s8S9uprvOAjwRcNQImk9uo3RhZ80CAqd2ZXJzaW9uAQ==

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/greem

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id greem
```